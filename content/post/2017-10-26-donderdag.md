---
title: 26 oct 2017 Donderdag
subtitle: Leerdossier
date: 2017-10-26
tags: ["blog", "school", "leerdossier"] 
---

Eindelijk na alle informatie over het leerdossier te hebben verzamelt kan ik beginnen met het leerdossier. Ik heb adviezen opgevolgd, vragen gesteld en workshops gevolgd. Dan zou ik nu toch wel kunnen beginnen met het maken van het leerdossier. Weet je, ik begin gewoon en dan zie ik wel verder. En gelukkig heb ik dat gedaan, nu ben ik bijna klaar en heb ik nog een hele dag om de puntjes op de i te zetten. Ik merk wel dat ik nog steeds heel veel vragen heb, ook al heb ik al zoveel informatie verzameld. Volgende keer moet ik eerder beginnen met het verwerken van deze informatie zodat ik geen moeite zal hebben met het schrijven van mijn leerdossier. Ik wil geen fouten maken dus ik doe alles volgens de regels en toch ben ik nog steeds bang dat het fout zal zijn.  Ik zal weer verder moeten en het loslaten, ik kan het alleen nog maar beter doen de volgende keer. Nog eventjes en dan vakantie… Ik mis vakantie…

Hieronder de beoordeling van het leerdossier. 

![](leerdossier.jpg)