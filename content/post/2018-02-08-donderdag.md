---
title: 08 feb 2018 donderdag
subtitle: Maastricht Part 2
date: 2018-02-08
tags: ["blog", "school", "maastriccht", "samenwerking", "carnaval"]
---

De samenwerking is heel interessant, omdat we deze in een uur op moesten bouwen en voor twee dagen aan moesten houden. Er waren geen regels waar iedereen zich aan moest houden, we deden wel aan deadlines en dat hielp de groep een beetje bij elkaar te houden. Ik merkte dat ik het moeilijk vond om iets te zeggen in de groep, omdat een paar groepsgenoten erg ongeduldig en dominant waren. Ik kwam niet met de juiste ideeën en dat vond ik jammer. Uiteindelijk heb ik aan mijn groepsgenoten gevraagd wat ze van de samenwerking vonden en ze vonden het jammer dat ze niet zo snel het woord kregen en dat de dominantere groepsleden alles bepaalden. Uiteindelijk hebben we met z’n allen afgesproken dat we het tegen hen zouden zeggen en dat we met z’n allen op een oplossing kunnen komen. Dat was goed gelukt en we kwamen met een verassende oplossing, waar veel mensen wel tevreden over waren. Helaas hebben we de prijs niet kunnen winnen, maar ik heb heel veel ervaring opgedaan als het gaat om de samenwerking. Uiteindelijk sloot ik de dag af met het leren kennen van nieuwe mensen en een leuk feestje. 