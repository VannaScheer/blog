---
title: 07 sept 2017 donderdag 
subtitle: Ideeën
date: 2017-09-07
tags: ["blog", "school", "vragen", "ideeën"]
---

Vandaag richt ik me meer op het spel en kom ik erachter dat ik mijn eigen bijdrage kan leveren doormiddel van vragen en ideeën. Wanneer ik een idee heb schrijf ik het binnen een minuut (oftewel zo snel mogelijk) op te schrijven zodat ik het niet meer vergeet. 

![](vraagteken.jpg)