---
title: 29 maa 2018 Donderdag
subtitle: School of awesome
date: 2018-03-29 
tags: ["blog","school","of","awesome"]
---

Het tentamen is voorbij en ondanks ik dit heel beroerd gepland heb, ging het tentamen erg goed. Dit komt waarschijnlijk, omdat ik de spiekbrief 10x opnieuw moest schrijven en ik het onbewust allemaal in mijn hoofd gestampt heb. Nu is het mijn doel om tot laat nog op school te zitten om de STARRTS af te maken. Zo hoef ik er thuis minder te doen. Aan de andere kant had ik ook gewoon een outlet nodig voor al mijn energie, had ik even LOL nodig en ondanks dat ik dus niet veel verder ben gekomen heb ik het wel heel erg naar mijn zin gehad. School gaat niet alleen om deadlines halen en al het saaie werk doen. Het gaat om nieuwe mensen leren kennen en jezelf te ontwikkelen tot een persoon die alle vaardigheden beheerst. Ook al lijkt het nu (door mijn beroerde planning) op dat ik alleen maar aan het werk ben, dat is echt niet zo. Ik doe deze opleiding, omdat ik het een leuke opleiding vind en niet omdat ik serieus wil gaan werken. Dus soms mag het wel, eventjes in je ergste week toch nog die LOL vinden. Want anders zou het maar een grauwe wereld zijn.

![](awesome.jpg)