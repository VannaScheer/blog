---
title: 05 sept 2017 dinsdag 
subtitle: het begin
date: 2017-09-05
tags: ["blog", "school", "handleiding", "beginning"]
---

Ik heb echt geen idee. Ik heb geen idee waar ik moet beginnen of wat ik moet doen. Waar is mijn handleiding? Waar is mijn lijstje zodat ik weet wat ik moet doen en waar ik me aan moet houden? Hoe weet ik wat goed en fout is? Ik ben hier zeker een vrije vogel, maar daar moet ik wel aan wennen. Ik wil een eigen handleiding maken met mijn eigen goed en fout, zodat ik precies weet wat ik moet doen. Ik heb dit gedaan door alles op een rijtje te zetten en ik zal nog moeten vragen of dit zou kunnen.

![](beginning.jpg)

