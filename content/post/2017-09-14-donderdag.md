---
title: 14 sept 2017 donderdag
subtitle: Beslisdag
date: 2017-09-14
tags: ["blog", "school", "beslissen", "twijfelen", "ervoorgaan"]
---

Vandaag was voor mij een beslisdag. In plaats van te dubben op hoe en wat ik moest doen, heb ik het gewoon gedaan. Zo zal ik niet alleen maar te twijfelen, maar kwam ik ook ergens. Ik heb alles op een rijtje gezet en ben gaan werken. Ik heb veel afgekregen, dus ik kan het advies geven dat de knoop doorhakken bij beslissingen echt helpt in een proces. Soms moet ik er gewoon voor gaan, in plaats van bedenken of het wel goed komt. 