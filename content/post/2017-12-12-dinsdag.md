---
title: 12 dec 2017 dinsdag
subtitle: Recap Part 1, Letterontwerp
date: 2017-12-12
tags: ["bolg", "school", "recap","letterontwerp"] 
---

Vandaag was het voor mij belangrijk dat ik de Recap af kreeg. Er werd een validatiemoment ingepland  om alle Recaps te bekijken. Ik heb geen idee hoe ik een Recap moet maken, ik weet niet eens waar ik moet beginnen. Ook waren er een aantal stukken die we misten die toch op een bepaalde manier in die Recap terecht moesten komen. Er kwamen een aantal vragen naar mij toe van mijn groepsgenoten of ze misschien konden helpen. Mijn verantwoordelijke verstand kon alleen maar bedenken: “Het is jouw taak, jij moet het afmaken. Zij moeten andere dingen afmaken, zij kunnen je niet helpen.” Achteraf gezien had ik daar totaal niet naar moeten luisteren en had ik dat stemmetje in mijn hoofd gewoon uit moeten zetten. Daar was ik op dat moment totaal niet over aan het denken. De tijd vloog voorbij en ik was de docenten stonden bij mijn tafel klaar om mij te valideren. Daar zaten we dan, zij te wachten tot ik iets ging zeggen en ik die niet wist wat ik moest zeggen. Uiteindelijk heb ik eruit gehaald dat ze met liefde nog even willen wachten totdat ik een iets betere versie heb. Dit keer ga ik zeker weten wat meer vinden over Recaps, voordat ik maar wat maak…

Letterontwerp was een interessante workshop, ik had er nog vrij weinig van gehoord en ik wist niet eens dat er een hele studie was voor alleen letterontwerp. Blijkbaar speelt het best een grote rol in het ontwerp van producten, met welke letterdesign overtuig ik de mensen het best. In de praktijk was het nog moeilijker dan in de theorie. Ik ben een perfectionist en ik wil alles goed hebben, hierom lukte het mij niet om een mooie letter te maken, terwijl het anderen beter af ging. Ik merkte wel dat de theorie toch best moeilijk was om in één keer alles te onthouden en door de les heen waren er heel veel vragen. Ik ben blij kennis gemaakt te hebben met een onderwerp waar ik nog weinig over gehoord had en waar ik iets mee kan als het gaat om de lay-out van dingen. Natuurlijk is het op de computer wel een stuk makkelijker dan met de hand, daar houd ik het dus nog even op. 

Hieronder de Recap en een Letterontwerp: 

![](recap.jpg)

![](letterontwerp.jpg)