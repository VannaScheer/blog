---
title: 21 oct 2017 Zaterdag
subtitle: Ontwerpen
date: 2017-10-21
tags: ["blog", "school", "ontwerpen", "huidig", "gewenst"]
---

Oké, mijn hele opleiding gaat over het ontwerpen van dingen. Dit kan van alles zijn, dat hoeft niet zo gauw digitaal. Nu moet ik een toekomstbeeld van de Kuip ontwerpen. De huidige situatie van de kuip is dat het veld niet zichtbaar is en dat het heel kalend is, toen ik er was kreeg ik niet de Feyenoord vibe (misschien ook wel omdat ik voor Sparta ben, maar dat terzijde). De gewenste situatie is als Feyenoord wat gezelliger wordt en waarin je het veld kan zien zelfs al zit je helemaal achterin. 
Oké schetsen en het ontwerp heb ik, maar hoe ga ik dit (de schets) in het echt neerzetten (het prototype). Na uren gefrustreerd alle mislukte Kuip propjes door de kamer te gooien, kwam ik erachter dat het niet zo gedetailleerd hoeft te zijn. Ik moet gewoon een goed verhaal erbij kunnen verzinnen, een verhaal dat mensen zal overtuigen dat dit een goed toekomstbeeld van de Kuip is. Ondanks dat het een goed idee is, gaat het er toch om dat ik het ook zichtbaar kan maken zonder het verhaal er perse bij te vertellen. Om dit te kunnen bereiken, merkte ik dat ik nog veel mocht oefenen.

![](kuip.jpeg)